s = "today is nice day, so we can go anywhere"
#a) Hãy đếm số ký tự trong chuỗi đã cho
print(len(s))

#b) Hãy xuất vị trí -5 đến -9
print(s[-5:-9])

#c) Xuất tất cả các ký tự trong chuỗi với bước nhảy là 3 
print(s[None:None:3])

#d) Hãy tách chuỗi đã cho thành chuỗi mới: 
s1 = print(s[0:17])
s2 = print(s[19:40])

#e) Hãy hợp nhất chuỗi s1 với s2 lại. Và sắp xếp theo chữ cái A->Z
print(sorted(s))

#f) chuyển các chuỗi sau khi hợp nhất thành chuỗi ký tự in hoa
print(s.upper())
