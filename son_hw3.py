#Example 1:
def sum(a,b):
  x = a+ b
  return x
a = 5
b = 9
print (sum(5,9))

#Example 2:
a = 5
b = 9
print((5*5)*(9*9))

#Example 3:
def index():
  s = 5
print(s)

#Example 4:
def index(a=5,b=9):
  return a+b
print(a+5)

#Example 5:
def index(a,b=9):
  return a+b

a=5
print(a)
    
